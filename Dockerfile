FROM alpine:3.10

RUN apk add --no-cache libintl gettext && \
    apk add --no-cache openssh ca-certificates curl && \
    apk add --no-cache ansible ansible-lint

RUN curl -L -o /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/latest.txt)/bin/linux/amd64/kubectl && \
    chmod +x /usr/bin/kubectl && \
    kubectl version --client
